**GeoMaps** / Journey Maps

  

1.  Geophysical Storymap (arc gis)
2.  Geospatial Value Map (Kumu)

  

**Mindmaps**

  

1\. HoloSphere ([gitmind](https://gitmind.com/app/doc/28299988))

[https://cloudflare-ipfs.com/ipfs/ZvETdEyG4EY548QYcwVn2QzZjisfRzjSwDtdHtYM9wHHyLBmpNcJYmWk2cQvdVsS4H3rWe9w2DK/HoloVerse.svg](https://cloudflare-ipfs.com/ipfs/ZvETdEyG4EY548QYcwVn2QzZjisfRzjSwDtdHtYM9wHHyLBmpNcJYmWk2cQvdVsS4H3rWe9w2DK/HoloVerse.svg)2\. HoloFlow ([gitmind](https://gitmind.com/app/doc/5cb99991))

[https://cloudflare-ipfs.com/ipfs/Z3fXC5XEqf6tJ7Yq5pYaqh9QwvEZQ84NXEKXBF1jgRNMtK9gbTt1oSecXNj36RNxZB1p18gv74z64/holosphere.svg](https://cloudflare-ipfs.com/ipfs/Z3fXC5XEqf6tJ7Yq5pYaqh9QwvEZQ84NXEKXBF1jgRNMtK9gbTt1oSecXNj36RNxZB1p18gv74z64/holosphere.svg)3\. HoloArchitecure (gitmind)

(placeholder)

4\. HoloEcosystem ([gitmind](https://gitmind.com/app/doc/a53bb2d945d7947a44b7f2e830dec2db))

[https://cloudflare-ipfs.com/ipfs/ZvETdEyG4EY548QY6VN8nvS74ThDzdY3cBNRDBZn7NJrsBjSzjpdRpKgs8ArkYx9unMDeBafTWZ/Ecosystem.svg](https://cloudflare-ipfs.com/ipfs/ZvETdEyG4EY548QY6VN8nvS74ThDzdY3cBNRDBZn7NJrsBjSzjpdRpKgs8ArkYx9unMDeBafTWZ/Ecosystem.svg)

**Graph Maps**

  

1.  Radial Graph of Holowiki (Graphiz) ([data sonification app](https://twotone.io/))

  

**Notes: Additional Mapping Layers**

  

Create Centralized, Decentralized, Localized Ownership Map

  

*   Servers
*   Software
*   Infrastructure
*   Legislation

  

Organic & Artificial Mapping Layer