[[#]] holoMaps (and the holoVerse Operating System)

holoMaps are the navigational tools to discover holoRings and holoAtoms w/i holoSpheres

our main repository is on [holoGit](hologit.ml), and is available on [IPFS][5]








### see also :

work of [Chris][1] (<https://holomap.earth/>) where :

* [holoSphere][2] == [holoWeb][3].
* holoRings == [holoForm][4]
* holomap-proton: <https://github.com/chrislarcombe/holomap-proton>

[1]: https://larcombe.io
[2]: https://holosphere.ml
[3]: https://youtu.be/L48VOGCxmuo
[4]: https://larcombe.io/2020/04/28/holoform-presentation/

[5]: https://gateway.ipfs.io/ipns/hologit.gq/holoverse/holomaps.gq
